angular.module('starter.controllers', [])
    .controller('AppCtrl', function($scope, $ionicModal, $ionicLoading, $timeout) {

        $ionicModal.fromTemplateUrl('template/register.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.modal = modal;
        });

        $ionicModal.fromTemplateUrl('template/forgotPassword.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.fmodal = modal;
        });

        $scope.registerData = {};
        $scope.forgotData = {};
        $scope.inputType = 'password';

        $scope.showPassword = function() {
            if ($scope.inputType == 'password') {
                $scope.inputType = 'text';
            } else {
                $scope.inputType = 'password';
            }
        }

        $scope.register = function() {
            console.log("test");
            $scope.modal.show();
        };

        // Triggered in the login modal to close it
        $scope.closeRegister = function() {
            $scope.modal.hide();
        };

        $scope.forgotPass = function() {
            console.log("test");
            $scope.fmodal.show();
        };

        $scope.closeForget = function() {
            $scope.fmodal.hide();
        }

        $scope.doRegister = function () {
            $scope.closeRegister();
            $scope.registerData = {};
        }

        $scope.getPassword = function () {
            $scope.closeForget();
            $scope.forgotData = {};
        }

        $scope.showLoader = function () {
            $ionicLoading.show({
                noBackdrop: true,
                template: '<div class="loaderDiv"><ion-spinner icon="crescent" class="spinner-dark"></ion-spinner><span>Loading...</span></div>'
            });
        }

        $scope.hideLoader = function () {
            $ionicLoading.hide();
        }
    })

    .controller('LoginCtrl', function($scope, $state, $http, $timeout) {
        $scope.loginData = {};
        $scope.inputType = 'password';

        $scope.showPassword = function() {
            if ($scope.inputType == 'password') {
                $scope.inputType = 'text';
            } else {
                $scope.inputType = 'password';
            }
        }

        $scope.doLogin = function () {
            $scope.showLoader();
            $http.post('http://180.149.246.126/trainingapp/api/users/login.json', {email: $scope.loginData.username, password: $scope.loginData.password})
                .then(function(resp) {
                    console.log('Success', resp);
                    $scope.loginData = {};
                    $scope.hideLoader();
                    $state.go('app.dashboard');
                    // For JSON responses, resp.data contains the result
                }, function(err) {
                    console.error('ERR', err);
                    $scope.hideLoader();
                    // err.status will contain the status code
                })
        }
    })

    .controller('DashboardCtrl', function($scope, $state, $timeout) {
        $scope.images = [
            "http://demos.telerik.com/kendo-ui/content/shared/images/photos/1.jpg",
            "http://demos.telerik.com/kendo-ui/content/shared/images/photos/2.jpg",
            "http://demos.telerik.com/kendo-ui/content/shared/images/photos/3.jpg",
            "http://demos.telerik.com/kendo-ui/content/shared/images/photos/4.jpg",
            "http://demos.telerik.com/kendo-ui/content/shared/images/photos/5.jpg",
            "http://demos.telerik.com/kendo-ui/content/shared/images/photos/6.jpg",
            "http://demos.telerik.com/kendo-ui/content/shared/images/photos/7.jpg",
            "http://demos.telerik.com/kendo-ui/content/shared/images/photos/8.jpg",
            "http://demos.telerik.com/kendo-ui/content/shared/images/photos/9.jpg",
            "http://demos.telerik.com/kendo-ui/content/shared/images/photos/10.jpg"
        ];

        $scope.logout = function() {
            $state.transitionTo('app.login');
        }

        $scope.goToListPage = function() {
            $state.go('app.listing');
        }
    })

    .controller('ListCtrl', function($scope, $state, $timeout) {
        $scope.goBack = function() {
            $state.transitionTo('app.dashboard');
        }
    })

